'''
Aleah Crawford
Final Project 331

'''

import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
import matplotlib.ticker as tick

def read_frame():
    return pd.read_csv("Border_Crossing_Entry_Data.csv", usecols = [0, 1, 3, 4, 5, 6], header=0)

def get_data(df):
    mex = 0
    can = 0
    for i in df.index:
        if df.loc[i, "Border"] == "US-Mexico Border":
            mex += int(df.loc[i, "Value"])
        elif df.loc[i, "Border"] == "US-Canada Border":
            can += int(df.loc[i, "Value"])
    return [mex, can]

def reformat_large_tick_values(tick_val, pos):
    """
    Turns large tick values (in the billions, millions and thousands) such as 4500 into 4.5K and also appropriately turns 4000 into 4K (no zero after the decimal).
    """
    if tick_val >= 1000000000:
        val = round(tick_val/1000000000, 1)
        new_tick_format = '{:}B'.format(val)
    elif tick_val >= 1000000:
        val = round(tick_val/1000000, 1)
        new_tick_format = '{:}M'.format(val)
    elif tick_val >= 1000:
        val = round(tick_val/1000, 1)
        new_tick_format = '{:}K'.format(val)
    elif tick_val < 1000:
        new_tick_format = round(tick_val, 1)
    else:
        new_tick_format = tick_val

    # make new_tick_format into a string value
    new_tick_format = str(new_tick_format)

    # code below will keep 4.5M as is but change values such as 4.0M to 4M since that zero after the decimal isn't needed
    index_of_decimal = new_tick_format.find(".")

    if index_of_decimal != -1:
        value_after_decimal = new_tick_format[index_of_decimal+1]
        if value_after_decimal == "0":
            # remove the 0 after the decimal point since it's not needed
            new_tick_format = new_tick_format[0:index_of_decimal] + new_tick_format[index_of_decimal+2:]

    return new_tick_format
            
        
   
def main():
    df = read_frame()
    lst = get_data(df)
    height = [lst[0], lst[1]]
    bars = ("US-MEXICO", "US-CANADA")
    y_pos = np.arange(len(bars))
    plt.xticks(y_pos, bars)
    plt.bar(y_pos, height)
    plt.ylabel("Total Number of Border Crossings")
    plt.title("Canada vs. Mexico Border Crossings")
    ax = plt.gca()
    ax.yaxis.set_major_formatter(tick.FuncFormatter(reformat_large_tick_values))
    plt.show()
    
    
if __name__ == "__main__":
    main()