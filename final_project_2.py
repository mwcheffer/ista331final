'''
Aleah Crawford
Final Project 331

'''

import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np
import matplotlib.ticker as tick

def read_frame():
    lst = []
    d = []
    v = []
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    v2 = []
    df = pd.read_csv("Border_Crossing_Entry_Data.csv", usecols = [0, 1, 3, 4, 5, 6], header=0)
    date = df["Date"]
    value = df["Value"]
    for i in date.values:
        lst.append(i[:10].replace(i[2:5], ""))
    s = pd.Series(value.values, lst)
    for item in s.index:
        if item not in d:
            d.append(item)
        else:
            continue
    for elem in d:
        total = 0
        for j in range(len(s.index)):
            if elem == s.index[j]:
                total += int(s.values[j])
        v.append(total)
    s2 = pd.Series(v, d)
    for k in s2.index:
        for l in range(len(months)):
            if int(k[:2]) == l + 1:
               k = months[l]
    return s2
    
def get_data(df):
    pass
    
def is_leap_year(year):
    '''
    Takes an integer year and returns True if it is a new year and False 
    otherwise
    '''
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False
 
def main():
    df = read_frame()
    print(df)
    
    
if __name__ == "__main__":
    main()  