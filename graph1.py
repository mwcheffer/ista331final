import pandas as pd 

def create_frame():
    return pd.read_csv("wine.csv")

def main():
    df = create_frame()
    print(df)

main()