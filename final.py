#df = pd.read_csv("athlete_events.csv")

# columns: 'ID', 'Name', 'Sex', 'Age', 'Height', 'Weight', 'Team', 'NOC', 'Games', 'Year', 'Season', 'City', 'Sport', 'Event', 'Medal'

# M    196594
# F     74522
# Name: Sex, dtype: int64

#Games?
import numpy as np, pandas as pd

def get_data():
    df = pd.read_csv("athlete_events.csv", usecols=['Sex','Age', 'Height', 'Weight', 'Year', 'Season', 'Sport', 'Event', 'Medal']).dropna(subset=['Age', 'Height', 'Weight'])
    X = df[['Sex','Age', 'Height', 'Weight', 'Year', 'Season', 'Sport', 'Medal']]
    y = df[['Event']]
    return pd.get_dummies(X),pd.get_dummies(y)

def get_train_and_test_sets(X,y):
    rand_array = np.random.permutation(X.shape[0])
    index = int(len(X) * 0.8)
    train, test = rand_array[:index], rand_array[index:]
    X_train, X_test = X.loc[train,:], X.iloc[test,:]
    y_train, y_test = y.iloc[train], y.iloc[test]
    return X_train, X_test, y_test, y_train

def train_to_data(X_train, y_train):
    